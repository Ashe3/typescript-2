import {fighterDetail, fighter} from '../types/types';

export interface IFighterService {

  getFighters(): Promise<fighter[]>;

  getFighterDetails(id: number): Promise<fighterDetail>;

}
