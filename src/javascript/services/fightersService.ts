import { fighter, fighterDetail } from '../types/types';
import { IFighterService } from './fightersServicesInterface';
import { callApi } from '../helpers/apiHelper';

class FighterService implements IFighterService {
  async getFighters(): Promise<fighter[]> {
    try {
      const endpoint: string = 'fighters.json';
      const apiResult: Promise<fighter[]> = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: number): Promise<fighterDetail> {
    try {
      const endpoint: string = `details/fighter/${id}.json`;
      const apiResult: Promise<fighterDetail> = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();