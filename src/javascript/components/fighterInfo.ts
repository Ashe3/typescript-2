import { fighterService } from "../services/fightersService";
import { fighterDetail } from "../types/types";

const fighterDetailsMap: Map<number, fighterDetail> = new Map();

export async function getFighterInfo(fighterId: number) {
  if (!fighterDetailsMap.has(fighterId)) {
    fighterDetailsMap.set(fighterId, await fighterService.getFighterDetails(fighterId));
  }

  return fighterDetailsMap.get(fighterId);
}