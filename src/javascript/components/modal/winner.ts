import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import { fighterDetail } from '../../types/types';

export function showWinnerModal(fighter: fighterDetail) {
  const { name, source: src } = fighter;
  const title = `${name} is a winner! Congrats!`;
  const bodyElement: HTMLElement = createElement({ tagName: 'img', className: 'modal-body', attributes: { src } });

  showModal({ title, bodyElement, onClose: () => document.location.reload() });
}
