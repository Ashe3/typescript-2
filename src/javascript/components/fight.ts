import { controls } from '../../constants/controls';
import { fighterDetail, makeHitFunc, obtFunction } from '../types/types';

export async function fight(
  { ...firstFighter }: fighterDetail,
  { ...secondFighter }: fighterDetail
): Promise<fighterDetail> {
  return new Promise((resolve) => {
    beforeStart(firstFighter, secondFighter);

    const firstFighterHealthBar = document.getElementById('left-fighter-indicator');
    const secondFighterHealthBar = document.getElementById('right-fighter-indicator');

    const hit: makeHitFunc = (attacker, defender, getCurrentDamage, bar, isCrit?): void => {
      if ((!attacker.isBlock && !defender.isBlock) || isCrit) {
        if (defender.currenthealth) {
          defender.currenthealth -= getCurrentDamage(attacker, defender);

          bar.style.width = defender.currenthealth >= 0 ? `${(defender.currenthealth / defender.health) * 100}%` : '0%';
        }
      }

      if (defender.currenthealth && defender.currenthealth <= 0) {
        resolve(attacker);
      }
    };

    if (firstFighterHealthBar && secondFighterHealthBar) {
      initListeneres(firstFighter, firstFighterHealthBar, secondFighter, secondFighterHealthBar, hit);
    }
  });
}

export function getDamage(attacker: fighterDetail, defender: fighterDetail): number {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return Math.max(0, damage);
}

export function getHitPower(fighter: fighterDetail): number {
  return fighter.attack * (Math.random() + 1);
}

export function getBlockPower(fighter: fighterDetail): number {
  return fighter.defense * (Math.random() + 1);
}

function getCriticalDamage(fighter: fighterDetail): number {
  return fighter.attack * 2;
}

function checkComboEquality(pressedCombo: string[], expectedCombo: string[]) {
  return expectedCombo.every((value) => pressedCombo.includes(value));
}

function collectCriticalCombo(fighter: fighterDetail, keyPressed: string) {
  if (!fighter.pressedCombo) fighter.pressedCombo = new Set();
  fighter.pressedCombo.has(keyPressed) ? fighter.pressedCombo.delete(keyPressed) : fighter.pressedCombo.add(keyPressed);
}

function tryToCriticalHit(
  attacker: fighterDetail,
  defender: fighterDetail,
  makeHit: makeHitFunc,
  bar: HTMLElement,
  keyPressed: string,
  combo: Array<string>
): void {
  collectCriticalCombo(attacker, keyPressed);

  const { isCriticalActive, isBlock } = attacker;

  if (attacker.pressedCombo) {
    const isCritCombo = checkComboEquality(Array.from(attacker.pressedCombo), combo);
    const delay = 10000; //ms

    if (isCritCombo && isCriticalActive && !isBlock) {
      makeHit(attacker, defender, getCriticalDamage, bar, true);
      attacker.isCriticalActive = false;
      setTimeout(() => (attacker.isCriticalActive = true), delay);
    }
  }
}

function beforeStart(firstFighter: fighterDetail, secondFighter: fighterDetail): void {
  firstFighter.currenthealth = firstFighter.health;
  secondFighter.currenthealth = secondFighter.health;
  firstFighter.isCriticalActive = true;
  secondFighter.isCriticalActive = true;
}

function initListeneres(
  firstFighter: fighterDetail,
  firstFighterHealthBar: HTMLElement,
  secondFighter: fighterDetail,
  secondFighterHealthBar: HTMLElement,
  hit: makeHitFunc
): void {
  document.addEventListener('keydown', (event) => {
    switch (event.code) {
      case controls.PlayerOneBlock:
        firstFighter.isBlock = true;
        break;
      case controls.PlayerTwoBlock:
        secondFighter.isBlock = true;
        break;
      default:
        if (controls.PlayerOneCriticalHitCombination.includes(event.code)) {
          tryToCriticalHit(
            firstFighter,
            secondFighter,
            hit,
            secondFighterHealthBar,
            event.code,
            controls.PlayerOneCriticalHitCombination
          );
        }
        if (controls.PlayerTwoCriticalHitCombination.includes(event.code)) {
          tryToCriticalHit(
            secondFighter,
            firstFighter,
            hit,
            firstFighterHealthBar,
            event.code,
            controls.PlayerTwoCriticalHitCombination
          );
        }
        break;
    }
  });

  document.addEventListener('keyup', (event) => {
    switch (event.code) {
      case controls.PlayerOneAttack:
        hit(firstFighter, secondFighter, getDamage, secondFighterHealthBar);
        break;
      case controls.PlayerOneBlock:
        firstFighter.isBlock = false;
        break;
      case controls.PlayerTwoAttack:
        hit(secondFighter, firstFighter, getDamage, firstFighterHealthBar);
        break;
      case controls.PlayerTwoBlock:
        secondFighter.isBlock = false;
        break;
      default:
        if (controls.PlayerOneCriticalHitCombination.includes(event.code)) {
          collectCriticalCombo(firstFighter, event.code);
        }
        if (controls.PlayerTwoCriticalHitCombination.includes(event.code)) {
          collectCriticalCombo(secondFighter, event.code);
        }
        break;
    }
  });
}
