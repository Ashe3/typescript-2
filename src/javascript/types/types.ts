export type fighter = {
  '_id': number,
  name: string,
  source: string
};

export type fighterDetail = fighter & {
  health: number,
  attack: number,
  defense: number
  currenthealth?: number,
  isCriticalActive?: boolean,
  isBlock?:boolean,
  pressedCombo?: Set<string>
};

export type obtFunction = (f1: fighterDetail, f2: fighterDetail) => number;

export type makeHitFunc = (a: fighterDetail, d: fighterDetail, getDamage: obtFunction, bar: HTMLElement, crit?: boolean) => void;